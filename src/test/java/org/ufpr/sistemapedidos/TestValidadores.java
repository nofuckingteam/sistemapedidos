package org.ufpr.sistemapedidos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.ufpr.sistemapedidos.validators.CPFValidador;
import org.ufpr.sistemapedidos.validators.ClienteValidador;
import org.ufpr.sistemapedidos.validators.PedidoValidador;
import org.ufpr.sistemapedidos.validators.ProdutoValidador;


public class TestValidadores {
	
	private final CPFValidador cpfValidador = new CPFValidador();
	private final ClienteValidador clienteValidador = new ClienteValidador();
	private final ProdutoValidador produtoValidador = new ProdutoValidador();

	@Test 
	public void testValidaClienteHappyDay() {
		validarCliente(new Cliente(0, "064.607.759-74", "Eric", "Rodrigo de Freitas"));
	}
	
	@Test(expected=RuntimeException.class)
	public void testClienteComNomeVazioLancaRuntimeException() {
		validarCliente(new Cliente(0, "064.607.759-74", "", "Rodrigo de Freitas"));
	}
	
	@Test(expected=RuntimeException.class)
	public void testClienteComNomeNuloLancaRuntimeException() {
		validarCliente(new Cliente(0, "064.607.759-74", null, "Rodrigo de Freitas"));
	}
	
	@Test(expected=RuntimeException.class)
	public void testClienteComNomeComMaisDeTrintaCaracteresLancaRuntimeException() {
		validarCliente(new Cliente(0, "064.607.759-74", "123456789123456789123456789123456789", "Rodrigo de Freitas"));
	}
	
	@Test(expected=RuntimeException.class)
	public void testClienteComSobreNomeVazioLancaRuntimeException() {
		validarCliente(new Cliente(0, "064.607.759-74", "Eric", ""));
	}
	
	@Test(expected=RuntimeException.class)
	public void testClienteComSobreNomeNullLancaRuntimeException() {
		validarCliente(new Cliente(0, "064.607.759-74", "Eric", null));
	}

	@Test(expected=RuntimeException.class)
	public void testClienteCPFFormatoInvalidoLancaRuntimeException() {
		validarCliente(new Cliente(0, "064.607.759-74asd", "Eric", "Rodrigo de Freitas"));
	}
	
	@Test(expected=RuntimeException.class)
	public void testClienteCPFDigitoInvalidoLancaRuntimeException() {
		validarCliente(new Cliente(0, "064.607.759-12", "Eric", "Rodrigo de Freitas"));
	}
	
	
	private void validarCliente(Cliente cliente)  {
		clienteValidador.validar(cliente);
	}
	
	@Test
	public void validaCPFCliente(){
		try {
			final Cliente cliente = new Cliente(0, "111.111.111.-11", "Eric", "Rodrigo de Freitas");
			validarCliente(cliente);
			fail();
		} catch (RuntimeException e) {
			assertEquals(ClienteValidador.CPF_INVALIDO, e.getMessage());
		}
	}

	@Test
	public void testValidaPedidoCenarioFeliz() throws ParseException {
		try {
			final List<ItemDoPedido> itensDoPedido = new ArrayList<ItemDoPedido>();
			itensDoPedido.add(new ItemDoPedido(new Produto(0, "Descri��o"), 1));
			final Cliente cliente = new Cliente(0, "064.607.759-74", "Eric", "Rodrigo de Freitas");
			final Pedido pedido = new Pedido(0, new SimpleDateFormat("dd/MM/yyyy").parse("15/08/2016"), cliente,  itensDoPedido);
			(new PedidoValidador()).validar(pedido);
		} catch (RuntimeException e) {
			fail();
		}
	}
	
	@Test
	public void testPedidoNuloLancaRuntimeExceptionNaValidacao(){
		try {
			(new PedidoValidador()).validar(null);
			fail();
		} catch (RuntimeException e) {
			assertEquals(PedidoValidador.PEDIDO_INVALIDO, e.getMessage());
		}
	}

	@Test
	public void testValidaDigitoVerificadorHappyDay() {
		String cpf = "546.471.429-49";
		validaCPF(cpf);
	}
	
	@Test
	public void testCPFSequenciasLancaExcecao(){
		int count = 0;
		for(int i = 0; i < 10; i++){
			String cpf = String.valueOf(i).replaceAll("[0-9]{1}", "$0$0$0.$0$0$0.$0$0$0-$0$0");
			try {
				validaCPF(cpf);
			} catch (RuntimeException e) {
				count++;
			}
		}
		assertEquals(10, count);
	}

	private void validaCPF(String cpf)  {
		cpfValidador.validar(cpf);
	}
	
	
	@Test(expected=RuntimeException.class)
	public void testValidaProdutoNulo(){
		produtoValidador.validar(null);
	}
	
	@Test(expected=RuntimeException.class)
	public void testValidaProdutoComDescricaoVazia(){
		produtoValidador.validar(new Produto(0, ""));
	}
	
	@Test(expected=RuntimeException.class)
	public void testValidaProdutoComDescricaoComEspacos(){
		produtoValidador.validar(new Produto(0, ""));
	}
	
	@Test(expected=RuntimeException.class)
	public void testValidaProdutoComDescricaoComMaisDe45Char(){
		produtoValidador.validar(new Produto(0, "1234567890123456789012345678901234567890123456"));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
