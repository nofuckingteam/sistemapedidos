package org.ufpr.sistemapedidos;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.ufpr.sistemapedidos.dao.ClienteDao;
import org.ufpr.sistemapedidos.dao.ConnectionFactory;
import org.ufpr.sistemapedidos.dao.PedidoDao;
import org.ufpr.sistemapedidos.dao.ProdutoDao;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class TestDatabase {
	private static final String SOBRE_NOME = "Rodrigo de Freitas";
	private static final String NOME = "Eric";
	private static final String CPF = "064.607.759-74";
	ClienteDao clienteDao = new ClienteDao();
	ProdutoDao produtoDao = new ProdutoDao();
	PedidoDao pedidoDao = new PedidoDao();

	@After
	public void after() throws ClassNotFoundException, SQLException, IOException, ParseException {
		limpaTabelaPedido();
		limpaTabelaProduto();
		limparTabelaCliente();
	}

	private void limpaTabelaPedido() throws ClassNotFoundException, SQLException, IOException, ParseException {
		List<Pedido> pedidos = pedidoDao.buscarTodos();
		for (Pedido pedido : pedidos) {
			pedidoDao.remover(pedido);
		}
	}

	private void limpaTabelaProduto() throws ClassNotFoundException, SQLException, IOException {
		List<Produto> list = produtoDao.buscarTodos();
		for (Produto produto : list) {
			produtoDao.remover(produto);
		}

		list = produtoDao.buscarTodos();
		assertTrue(list.size() == 0);
	}

	private void limparTabelaCliente() throws ClassNotFoundException, SQLException, IOException {
		List<Cliente> list = clienteDao.buscarTodos();
		for (Cliente cliente : list) {
			clienteDao.remover(cliente);
		}

		list = clienteDao.buscarTodos();
		assertTrue(list.size() == 0);
	}

	@Test
	public void testConnection() throws ClassNotFoundException, SQLException, IOException {
		ConnectionFactory factory = new ConnectionFactory();
		Connection connection = factory.getConnection();
		assertNotNull(connection);
	}

	@Test
	public void testeManterCliente() throws ClassNotFoundException, SQLException, IOException {
		testInserirCliente();
		List<Cliente> list = clienteDao.buscarTodos();
		Cliente cliente = list.get(0);
		String cpf = "111.111.111-11";
		String nome = "Joana";
		String sobreNome = "Dark";
		cliente.setNome(nome);
		cliente.setSobreNome(sobreNome);
		cliente.setCpf(cpf);
		clienteDao.alterar(cliente);
		Cliente comparador = clienteDao.buscarPorId(cliente.getId());
		assertNotNull(comparador);
		assertEquals(cpf, comparador.getCpf());
		assertEquals(nome, comparador.getNome());
		assertEquals(sobreNome, comparador.getSobreNome());
	}

	@Test
	public void testInserirCliente() throws ClassNotFoundException, SQLException, IOException {
		clienteDao.inserir(new Cliente(0, CPF, NOME, SOBRE_NOME));
		List<Cliente> list = clienteDao.buscarTodos();
		assertTrue(list.size() > 0);
	}

	@Test(expected = MySQLIntegrityConstraintViolationException.class)
	public void testeClienteCPFUnico() throws ClassNotFoundException, SQLException, IOException {
		Cliente cliente = new Cliente(0, CPF, NOME, SOBRE_NOME);
		clienteDao.inserir(cliente);
		clienteDao.inserir(cliente);
	}

	@Test
	public void testInserirProduto() throws ClassNotFoundException, SQLException, IOException {
		produtoDao.inserir(new Produto(0, "Produto"));
		List<Produto> list = produtoDao.buscarTodos();
		assertTrue(list.size() > 0);
	}

	@Test
	public void testAlterarProduto() throws ClassNotFoundException, SQLException, IOException {
		produtoDao.inserir(new Produto(0, "Produto"));
		List<Produto> list = produtoDao.buscarTodos();
		assertEquals(1, list.size());
		Produto produto = list.get(0);
		String descricao = "Produto2";
		produto.setDescricao(descricao);
		produtoDao.alterar(produto);
		Produto comparador = produtoDao.buscarPorId(produto.getId());
		assertEquals(descricao, comparador.getDescricao());
	}

	@Test
	public void testInserirPedido() throws ClassNotFoundException, Exception, IOException {
		preparaAmbienteParaInserirPedido();
		Cliente cliente = clienteDao.buscarTodos().get(0);
		ArrayList<ItemDoPedido> itensDoPedido = new ArrayList<ItemDoPedido>();
		List<Produto> produtos = produtoDao.buscarTodos();
		for (Produto produto : produtos) {
			itensDoPedido.add(new ItemDoPedido(produto, produtos.indexOf(produto)));
		}

		pedidoDao.inserir(new Pedido(0, new SimpleDateFormat("dd/MM/yyyy").parse("20/08/2016"), cliente, itensDoPedido));
		assertTrue(pedidoDao.buscarTodos().size() > 0);
	}

	private void preparaAmbienteParaInserirPedido() throws ClassNotFoundException, SQLException, IOException {
		clienteDao.inserir(new Cliente(0, CPF, NOME, SOBRE_NOME));
		produtoDao.inserir(new Produto(0, "Produto 1"));
		produtoDao.inserir(new Produto(0, "Produto 2"));
		produtoDao.inserir(new Produto(0, "Produto 3"));
	}

	@Test
	public void testBuscaCllientePorCPF() throws ClassNotFoundException, SQLException, IOException {
		testInserirCliente();
		assertNotNull(clienteDao.buscaPorCPF(CPF));
	}
}
