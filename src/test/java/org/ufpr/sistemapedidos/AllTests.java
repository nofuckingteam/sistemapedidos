package org.ufpr.sistemapedidos;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestDatabase.class, TestValidadores.class })
public class AllTests {

}
