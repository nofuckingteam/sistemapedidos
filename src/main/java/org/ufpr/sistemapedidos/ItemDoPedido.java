package org.ufpr.sistemapedidos;

public class ItemDoPedido {

	private Produto produto;
	private int quantidade;

	
	public ItemDoPedido(){}
	public ItemDoPedido(Produto produto, int quantidade) {
		this.produto = produto;
		this.quantidade = quantidade;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

}
