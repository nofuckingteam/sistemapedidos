package org.ufpr.sistemapedidos;

import java.util.Date;
import java.util.List;


public class Pedido {

	private int id;
	private Date data;
	private List<ItemDoPedido> itensDoPedido;
	private Cliente cliente;

	public Pedido(){}
	
	public Pedido(int id, Date data2, Cliente cliente, List<ItemDoPedido> itensDoPedido) {
		this.id = id;
		this.data = data2;
		this.cliente = cliente;
		this.itensDoPedido = itensDoPedido;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public List<ItemDoPedido> getItensDoPedido() {
		return itensDoPedido;
	}

	public void setItensDoPedido(List<ItemDoPedido> itensDoPedido) {
		this.itensDoPedido = itensDoPedido;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@Override
	public String toString() {
		return String.format("%1$09d", id);
	}

}
