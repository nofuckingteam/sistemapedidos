package org.ufpr.sistemapedidos.validators;

import static java.lang.Character.getNumericValue;

public class CPFValidador implements Validador<String> {

	public  static final String CPF_INVALIDO = "CPF inv�lido";

	private final String regex = "[0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2}";
	
	public void validar(String cpf)  {
		verificaFormato(cpf);
		String parseCPF = parseCPF(cpf);
		validaSequencias(parseCPF);
		validaDigitoVerificador(parseCPF);
	}
	
	private void verificaFormato(String cpf)  {
		if (!cpf.matches(regex))
			throw new RuntimeException(CPF_INVALIDO);
	}

	private String parseCPF(String cpf) {
		return cpf.replaceAll("[^\\d]", "");
	}

	private void validaSequencias(String cpf)  {
		for (int i = 0; i < 10; i++) {
			String matcher = ("" + i).replaceAll("[0-9]{1}", "$0$0$0$0$0$0$0$0$0$0$0");
			if (cpf.matches(matcher)) {
				throw new RuntimeException(CPF_INVALIDO);
			}
		}
	}

	private void validaDigitoVerificador(String cpf)  {
		validaPrimeiroDigitoVerificador(cpf);
		validaSegundoDigitoVerificador(cpf);
	}
	
	private void validaPrimeiroDigitoVerificador(String cpf)  {
		validaDigito(cpf, 9, calculaPrimeiroDigitoVerificador(cpf));
	}
	
	private void validaDigito(String cpf, int i, int digitoVerificador)  {
		if (digitoVerificador != getNumericValue(cpf.charAt(i)))
			throw new RuntimeException(CPF_INVALIDO);
	}
	
	private int calculaPrimeiroDigitoVerificador(String cpf) {
		return calculaDigitoVerificador(cpf, 10);
	}
	
	private int calculaDigitoVerificador(String cpf, int peso) {
		int soma = 0;
		int c = peso;
		for (int i = 0; i <= c; i++)
			soma += peso > 1 ? getNumericValue(cpf.charAt(i)) * peso-- : 0;

		return 11 - (soma % 11);
	}

	private void validaSegundoDigitoVerificador(String cpf)  {
		validaDigito(cpf, 10, calculaSegundoDigitoVerificador(cpf));
		
	}

	private int calculaSegundoDigitoVerificador(String cpf) {
		return calculaDigitoVerificador(cpf, 11);
	}
	
	
	

	

	

}
