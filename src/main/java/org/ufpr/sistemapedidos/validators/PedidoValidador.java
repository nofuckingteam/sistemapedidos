package org.ufpr.sistemapedidos.validators;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.ufpr.sistemapedidos.Cliente;
import org.ufpr.sistemapedidos.ItemDoPedido;
import org.ufpr.sistemapedidos.Pedido;

public class PedidoValidador implements Validador<Pedido> {

	public static final String PEDIDO_INVALIDO = "Pedido inv�lido";

	public void validar(Pedido pedido) {
		validaPedido(pedido);
		validaDataDoPedido(pedido.getData());
		validaClienteDoPedido(pedido.getCliente());
		validaItensDoPedido(pedido.getItensDoPedido());
	}

	private void validaItensDoPedido(List<ItemDoPedido> itensDoPedido) {
		if (itensDoPedido == null || itensDoPedido.size() == 0)
			throw new RuntimeException(PEDIDO_INVALIDO);
		for (ItemDoPedido itemDoPedido : itensDoPedido) {
			if (itemDoPedido.getQuantidade() < 1)
				throw new RuntimeException(PEDIDO_INVALIDO);
			if (itemDoPedido.getProduto() == null)
				throw new RuntimeException(PEDIDO_INVALIDO);
		}
	}

	private void validaClienteDoPedido(Cliente cliente) {
		(new ClienteValidador()).validar(cliente);
	}

	private void validaPedido(Pedido pedido) {
		if (pedido == null)
			throw new RuntimeException(PEDIDO_INVALIDO);
	}

	private void validaDataDoPedido(Date date) {
		if (date == null)
			throw new RuntimeException(PEDIDO_INVALIDO);
	}
}
