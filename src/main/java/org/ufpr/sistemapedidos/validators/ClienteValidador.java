package org.ufpr.sistemapedidos.validators;

import org.ufpr.sistemapedidos.Cliente;

public class ClienteValidador implements Validador<Cliente> {

	public static final String NOME_INVALIDO = "Nome inv�lido";
	public static final String CPF_INVALIDO = "CPF inv�lido";
	public static final String SOBRENOME_INVALIDO = "Sobrenome inv�lido";
	public static final String CLIENTE_INVALIDO = "Cliente inv�lido";

	public void validar(Cliente cliente) {
		if (cliente == null)
			throw new RuntimeException(CLIENTE_INVALIDO);
		new CPFValidador().validar(cliente.getCpf());
		validaNome(cliente.getNome());
		validaSobreNome(cliente.getSobreNome());
	}

	private void validaSobreNome(String sobreNome) {
		if (sobreNome == null)
			throw new RuntimeException(SOBRENOME_INVALIDO);
		if (sobreNome.trim().length() <  1) throw new RuntimeException(SOBRENOME_INVALIDO);
		if(sobreNome.trim().length() >  50) throw new RuntimeException(SOBRENOME_INVALIDO);
		
	}

	private void validaNome(String nome){
		if (nome == null) throw new RuntimeException(NOME_INVALIDO);
		if (nome.trim().length() < 1) throw new RuntimeException(NOME_INVALIDO);
		if (nome.trim().length() > 10) throw new RuntimeException(NOME_INVALIDO);
	}
}


