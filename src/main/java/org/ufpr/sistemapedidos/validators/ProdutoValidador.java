package org.ufpr.sistemapedidos.validators;

import org.ufpr.sistemapedidos.Produto;

public class ProdutoValidador implements Validador<Produto> {

	private static final String PRODUTO_INVALIDO = "Produto inválido";

	@Override
	public void validar(Produto t){
		if (t == null 
				|| t.getDescricao() == null 
				|| "".equals(t.getDescricao().trim())
				|| t.getDescricao().length() > 45)
			throw new RuntimeException(PRODUTO_INVALIDO);

	}

}
