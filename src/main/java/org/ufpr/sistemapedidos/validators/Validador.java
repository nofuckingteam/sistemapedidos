package org.ufpr.sistemapedidos.validators;

public interface Validador<T> {

	public void validar(T t);
}
