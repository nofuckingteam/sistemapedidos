package org.ufpr.sistemapedidos.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.ufpr.sistemapedidos.Cliente;

public class ClienteDao implements DAO<Cliente>{
	ConnectionFactory factory = new ConnectionFactory();
	
	public void inserir(Cliente cliente) throws ClassNotFoundException, SQLException, IOException {
		Connection connection = factory.getConnection();
		String sql = "insert into cliente (cpf, nome, sobreNome) VALUES(?,?,?)";
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setString(1, cliente.getCpf());
		statement.setString(2, cliente.getNome());
		statement.setString(3, cliente.getSobreNome());
		statement.executeUpdate();
		statement.close();
		connection.close();

	}
	
	public void alterar(Cliente cliente) throws ClassNotFoundException, SQLException, IOException {
		Connection connection = factory.getConnection();
		String sql = "update cliente set cpf = ? , nome = ? , sobreNome = ? where id = ?";
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setString(1, cliente.getCpf());
		statement.setString(2, cliente.getNome());
		statement.setString(3, cliente.getSobreNome());
		statement.setInt(4, cliente.getId());
		statement.executeUpdate();
		statement.close();
		connection.close();

	}
	
	public void remover(Cliente cliente) throws ClassNotFoundException, SQLException, IOException{
		Connection connection = factory.getConnection();
		String sql = "delete from cliente where id = ?";
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setInt(1, cliente.getId());
		statement.executeUpdate();
		statement.close();
		connection.close();
	}
	
	public Cliente buscarPorId(int id) throws ClassNotFoundException, SQLException, IOException{
		Connection connection = factory.getConnection();
		String sql = "select id, cpf, nome, sobreNome from cliente where id = ?";
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setInt(1, id);
		ResultSet rs = statement.executeQuery();
		Cliente cliente = null;
		while(rs.next()){
			cliente = getClientFromRow(rs);
		}
		statement.close();
		connection.close();
		return cliente;
	}

	private Cliente getClientFromRow(ResultSet rs) throws SQLException {
		return new Cliente(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
	}
	
	public List<Cliente> buscarTodos() throws ClassNotFoundException, SQLException, IOException{
		Connection connection = factory.getConnection();
		String sql = "select id, cpf, nome, sobreNome from cliente";
		PreparedStatement statement = connection.prepareStatement(sql);
		ResultSet set = statement.executeQuery();
		List<Cliente> clientes = new ArrayList<Cliente>();
		while(set.next()){
			clientes.add(new Cliente(set.getInt(1), set.getString(2), set.getString(3), set.getString(4)));
		}
		statement.close();
		connection.close();
		return clientes;
	}

	public Cliente buscaPorCPF(String cpf) throws ClassNotFoundException, SQLException, IOException {
		Connection connection = factory.getConnection();
		PreparedStatement statement = connection.prepareStatement("select id, cpf, nome, sobrenome from cliente where cpf = ?");
		statement.setString(1, cpf);
		ResultSet rs = statement.executeQuery();
		if(rs.next()){
			return getClientFromRow(rs);
		}
		return null;
	}
}
