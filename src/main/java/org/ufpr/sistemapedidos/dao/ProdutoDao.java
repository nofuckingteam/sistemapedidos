package org.ufpr.sistemapedidos.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.ufpr.sistemapedidos.Produto;

public class ProdutoDao implements DAO<Produto>{
	ConnectionFactory factory = new ConnectionFactory();

	public void inserir(Produto produto) throws ClassNotFoundException, SQLException, IOException {
		Connection connection = factory.getConnection();
		PreparedStatement statement = connection.prepareStatement("insert into produto (descricao) values (?)");
		statement.setString(1, produto.getDescricao());
		statement.executeUpdate();
		statement.close();
		connection.close();
	}

	public List<Produto> buscarTodos() throws ClassNotFoundException, SQLException, IOException {
		Connection connection = factory.getConnection();
		PreparedStatement statement = connection.prepareStatement("select id, descricao from produto");
		ResultSet rs = statement.executeQuery();
		List<Produto> list = new ArrayList<Produto>();
		while(rs.next()){
			list.add(new Produto(rs.getInt(1), rs.getString(2)));
		}
		rs.close();
		statement.close();
		connection.close();
		return list;
	}

	public void remover(Produto produto) throws ClassNotFoundException, SQLException, IOException {
		Connection connection = factory.getConnection();
		PreparedStatement statement = connection.prepareStatement("delete from produto where id = ?");
		statement.setInt(1, produto.getId());
		statement.executeUpdate();
		statement.close();
		connection.close();
	}

	public void alterar(Produto produto) throws ClassNotFoundException, SQLException, IOException {
		Connection connection = factory.getConnection();
		PreparedStatement statement = connection.prepareStatement("update produto set descricao = ? where id = ?");
		statement.setString(1, produto.getDescricao());
		statement.setInt(2, produto.getId());
		statement.executeUpdate();
		statement.close();
		connection.close();
	}

	public Produto buscarPorId(int id) throws ClassNotFoundException, SQLException, IOException {
		Connection connection = factory.getConnection();
		PreparedStatement statement = connection.prepareStatement("select id, descricao from produto where id = ?");
		statement.setInt(1, id);
		ResultSet resultSet = statement.executeQuery();
		resultSet.next();
		Produto produto = new Produto(resultSet.getInt(1), resultSet.getString(2));
		resultSet.close();
		statement.close();
		connection.close();
		return produto;
		
	}

}
