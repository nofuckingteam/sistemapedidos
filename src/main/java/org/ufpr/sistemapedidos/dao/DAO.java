package org.ufpr.sistemapedidos.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface DAO<T> {
	public void inserir(T t) throws ClassNotFoundException, SQLException, IOException;
	public void alterar(T t) throws ClassNotFoundException, SQLException, IOException;
	public void remover(T t) throws ClassNotFoundException, SQLException, IOException;
	public List<T> buscarTodos() throws ClassNotFoundException, SQLException, IOException;
	public T buscarPorId(int id) throws ClassNotFoundException, SQLException, IOException;
}
