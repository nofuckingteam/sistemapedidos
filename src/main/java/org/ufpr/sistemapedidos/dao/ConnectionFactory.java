package org.ufpr.sistemapedidos.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionFactory {

	public Connection getConnection() throws ClassNotFoundException, SQLException, IOException {
		
		Properties properties = loadProperties();
		
		String driverClassName = properties.getProperty("driver.class");
		String url = properties.getProperty("driver.url");
		String username = properties.getProperty("driver.username");
		String password = properties.getProperty("driver.password");
		
		Class.forName(driverClassName);
		return DriverManager.getConnection(url, username, password);
	}

	private Properties loadProperties() throws IOException {
		InputStream stream = getClass().getClassLoader().getResourceAsStream("config.properties");
		Properties properties = new Properties();
		properties.load(stream);
		return properties;
	}

}
