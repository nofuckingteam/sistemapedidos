package org.ufpr.sistemapedidos.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.ufpr.sistemapedidos.Cliente;
import org.ufpr.sistemapedidos.ItemDoPedido;
import org.ufpr.sistemapedidos.Pedido;
import org.ufpr.sistemapedidos.Produto;

import com.mysql.jdbc.Statement;

public class PedidoDao implements DAO<Pedido>{
	ConnectionFactory factory = new ConnectionFactory();
	private ClienteDao clienteDao = new ClienteDao();

	public void inserir(Pedido pedido) throws ClassNotFoundException, SQLException, IOException {
		Connection connection = factory.getConnection();
		
		String sql = "insert into pedido (data, id_cliente) values (now(), ?)";
		PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		statement.setInt(1, pedido.getCliente().getId());
		statement.executeUpdate();
		ResultSet generatedKeys = statement.getGeneratedKeys();
		if(generatedKeys.next()){
			pedido.setId(generatedKeys.getInt(1));
		}
		generatedKeys.close();
		statement.close();
		for (ItemDoPedido item : pedido.getItensDoPedido()) 		{
			statement = connection.prepareStatement("insert into item_do_pedido (id_pedido, id_produto, quantidade) values (?,?,?)");
			statement.setInt(1, pedido.getId());
			statement.setInt(2, item.getProduto().getId());
			statement.setInt(3, item.getQuantidade());
			statement.executeUpdate();
			statement.close();
		}
		
		connection.close();
	}

	public List<Pedido> buscarTodos() throws ClassNotFoundException, SQLException, IOException{
		Connection connection = factory.getConnection();
		PreparedStatement statement = connection.prepareStatement("select id, data, id_cliente from pedido");
		return executaStatementRetornandoPedidos(connection, statement);
	}
	
	public List<Pedido> buscaPorCpf(String cpf) throws ClassNotFoundException, SQLException, IOException{
		Connection connection = factory.getConnection();
		PreparedStatement statement = connection.prepareStatement("select p.id, p.data, p.id_cliente from pedido p join cliente c on p.id_cliente = c.id where c.cpf = ?");
		
		statement.setString(1, cpf);
		return executaStatementRetornandoPedidos(connection, statement);
	}

	private List<Pedido> executaStatementRetornandoPedidos(Connection connection, PreparedStatement statement)
			throws SQLException, ClassNotFoundException, IOException {
		List<Pedido> pedidos = new ArrayList<Pedido>();
		ResultSet rs = statement.executeQuery();
		while(rs.next()){
			int id = rs.getInt(1);
			Date data = null;
			try {
				data = new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString(2));
			} catch (ParseException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			Cliente cliente = clienteDao.buscarPorId(rs.getInt(3));
			List<ItemDoPedido> itensDoPedido = buscarItensDoPedido(id, connection);
			pedidos.add(new Pedido(id, data, cliente, itensDoPedido));
		}
		rs.close();
		statement.close();
		connection.close();
		
		return pedidos;
	}

	private List<ItemDoPedido> buscarItensDoPedido(int id, Connection connection) throws SQLException {
		PreparedStatement statement = connection.prepareStatement("select p.id, p.descricao, idp.quantidade from item_do_pedido idp inner join produto p on idp.id_produto"
				+ " = p.id where idp.id_pedido = ?");
		statement.setInt(1, id);
		ResultSet rs = statement.executeQuery();
		ArrayList<ItemDoPedido> list = new ArrayList<ItemDoPedido>();
		while(rs.next()){
			int idProduto = rs.getInt(1);
			String descricao = rs.getString(2);
			int quantidade = rs.getInt(3);
			list.add(new ItemDoPedido(new Produto(idProduto, descricao), quantidade));
		}
		rs.close();
		statement.close();
		return list;
	}

	public void remover(Pedido pedido) throws ClassNotFoundException, SQLException, IOException {
		Connection connection = factory.getConnection();
		PreparedStatement statement = null;
		for(ItemDoPedido item : pedido.getItensDoPedido()){
			statement = connection.prepareStatement("delete from item_do_pedido where id_pedido = ? and id_produto = ?");
			statement.setInt(1, pedido.getId());
			statement.setInt(2, item.getProduto().getId());
			statement.executeUpdate();
			statement.close();
		}
		
		statement = connection.prepareStatement("delete from pedido where id = ?");
		statement.setInt(1, pedido.getId());
		statement.executeUpdate();
		statement.close();
		connection.close();
		
	}

	public List<Pedido> buscarPedidosDoCliente(String cliente) throws ClassNotFoundException, SQLException, IOException {
		return null;
	}

	@Override
	public void alterar(Pedido t) throws ClassNotFoundException, SQLException, IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Pedido buscarPorId(int id) throws ClassNotFoundException, SQLException, IOException {
		// TODO Auto-generated method stub
		return null;
	}
}
